## 开源：Apache2开源协议发布，并提供免费使用。（免费白嫖，欢迎欢迎 ）

## 由于工作繁忙 此项目不进行维护 仅供学习参考 和 二次模板开发 
  + 此后台管理系统 技术栈过于老 仅限于学习和参考 去拥抱新技术 php8  vue3 ... 等

## 教程视频：[首页B站](https://space.bilibili.com/258238619) `https://space.bilibili.com/258238619`
  + [01-搭建项目视频教程](https://www.bilibili.com/video/BV18R4y1M7DJ)  `https://www.bilibili.com/video/BV18R4y1M7DJ`
  + [02-搭建小程序教程](https://www.bilibili.com/video/BV1V8411s7S2) `https://www.bilibili.com/video/BV1V8411s7S2`
  + [03-商品sku 批量商品评论等操作](https://www.bilibili.com/video/BV1bG41177FH) `https://www.bilibili.com/video/BV1bG41177FH`
  + [04-视频操作 批量视频评论等操作](https://www.bilibili.com/video/BV1QW4y1J7t7)  `https://www.bilibili.com/video/BV1QW4y1J7t7`
  + [05-此项目已不再维护 期待我的下一个项目吧 开发神器 前后端 产品 测试 都可使用]

## 一条龙服务项目 定位：社交+短视频+商城+sass系统 (来白嫖) 
  + [管理系统](https://gitee.com/user_ye/zeng_admin)：`https://gitee.com/user_ye/zeng_admin` **就是这个项目**
    + 演示站点：`https://madmin.zengye.top/admin/login` 账号：`xiaozeng` 密码：`123456`
  + [api接口-app小程序专用](https://gitee.com/user_ye/zeng_api): `https://gitee.com/user_ye/zeng_api`
  + [小程序代码](https://gitee.com/user_ye/Mall-video): `https://gitee.com/user_ye/Mall-video`
  + 因为后台管理系统 `https://madmin.zengye.top` 走了cdn并且设置了防盗链 所以需要自己复制地址（游览器输入）进行访问 ，如果是从码云或其他地方跳转的会访问不了 

## 后台管理系统展示图：
  + 演示地址：[管理后台系统](https://madmin.zengye.top/admin/login) （里面的数据 随意操作）
  + 演示站点域名：`https://madmin.zengye.top/admin/login`
  + 因为域名走了cdn和并且添加了防盗链 直接从码云跳转过去可能会访问不了，建议复制链接直接新开游览器窗口打开
  + 账号：`xiaozeng` 密码：`123465`
  + ![展示图](./demo_img/1.png);
  + ![演示图](./demo_img/1.gif)
  + api接口项目演示图： （如需了解更多 请查看上面的git项目地址）
    + ![api接口项目演示图](./demo_img/api/1.gif)
    + [api接口文档html](https://madmin.zengye.top/static/docs/zengji.html) `https://madmin.zengye.top/static/docs/zengji.html`


## 为什么开源？（希望中国能向西方工会学习，受了某些‘良心’老板的欠薪，严重刨削，敢于站出来维护打工人的利益，前人种树，后人乘凉）
  + 公司名称：`喵美美（广州）网络科技有限公司` 于2020年10月低 欠薪跑路 [企业查链接](https://www.tianyancha.com/brand/b306e612764)
  + 宋林青 （已被劳动仲裁，列为失信人） 我们员工等11人 通过法院 法院要求宋林青执行支付工资 ，但宋林青不执行 （法院只能将其列为失信人员，我们员工等11人没有任何的赔偿和保障）
  + 宋林青 于2021-07月多创建了 `广东宝诗龙公司` 广州白云区 （没有查到法人和股东的信息） 听闻在里面
  + 在中国 老板欠薪跑路 我们普通员工 就算打赢了官司（劳动仲裁赢了）还是一分钱都拿不到 也没有任何保障
  + 为什么拖到现在开源，反抗 （我本是一个老实人，想着如何做好自己的本职工作，受点欺负，受点气也忍了 后面越忍  老板，领导就越往里面整）希望受到不公平的事情勇于反抗 （前人种树后人乘凉 反抗多了 资本家，老板就不会理所当然理直气壮 对比下西方的工会就知道 中国的打工有多不公平）
  + 晚点会打码上传法院裁决书和信息

## 仲裁证据（是一位小姐姐同事帮忙拍照的,姓名打码尊重同事的隐私）
  + ![展示图](./demo_img/zhengju/ma.gif)

## 欢迎白嫖 欢迎交流 联系方式如下：（主微信，qq很少上）
  + 微信：`zeng1569838235` 
  + qq: `1459584248`
  
### 如果觉得项目不错 ，麻烦动动你的小手指 花费一毛钱的短信 催 `宋林青 号码：13922314913` 发送：
  + `请尽快结清广州喵美美所欠员工的薪资`  
  + 只要发送了 有任何搭建或二开问题 我第一时间回复  亲力亲为帮你搭建好环境 项目

## 项目搭建  这里是后台管理系统 还有小程序api项目(tp6) 小程序代码等 待开源和补充文档完善
  + php版本：`5.4-7.2` 超过7.2版本某些功能和函数会报错 建议7.2版本
  + 需要安装redis，并启动
  + 创建数据库
  + 执行最新版的sql文件：[zeng.sql](./sql_back/zeng.sql)
  + 配置数据库，文件路径： `/application/database.php`
  + 配置域名运行目录为 `public`
  + 需要创建日志目录 `runtime` 记得给权限 0755
  + 需要创建图片上传文件夹 `public/newUploads` 记得给权限 0755
  + 需要创建临时存储的图片上传文件夹 `public/uploads/video_img` 记得给权限 0755
  + 需要配置伪静态：此处为nginx
  + 登录： 账号：`xiaozeng` 密码：`123456`

### 配置文件 （里面有写很详细的配置注释说明） 
  + 需要将 `env.ini.example` 复制一份到 `.env` 文件
  + 自行修改mysql 和 redis cos 等
   
```nginx
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}
```

  + 登录页面：`/admin/login`
